package executor;

import template.Template;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
//import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		String file_of_patterns = "";

		// Read input parameters
	    for (int i=0; i<args.length; i++){
			if (args[i].equals("-file")) 		file_of_patterns = args[++i];
		}
	    
	    Template T = new Template();
//	    ArrayList<String> patterns = new ArrayList<String>();
	    
		 try {
		 String line;
		 BufferedReader reader = new BufferedReader(new FileReader(file_of_patterns));
		 while ((line = reader.readLine()) != null) {
		        T.addPattern(line);
		    }
		    reader.close();
		 } catch (IOException e) { e.printStackTrace(); }
		 
//		 T.addKleeneEdge("29", "17", "0", "none");
		 
		 T.cleanupAnnotations();
		 
		 //System.out.println(T);
		 //System.out.println("---------------");
		 T.checkAnnotations();
	}
}
